# Blum Blum Shub Pseudorandom Number Generator

## Features

* Generate a random seed (only a prime number with a given length set in js, doesn't verify co-primality)
* Select how many numbers to generate from the cycle created by the `seed`, `p` and `q`
* Find a generated `x` by it's `index` (using Euler's Theorem) in the cycle created at the previous step

## This project uses:

* [jQuery **v3.3.1**](https://jquery.com/download/)
* [Bootstrap **v4.0**](https://getbootstrap.com/docs/4.0/getting-started/download/)
* [BigNumberJS **v8.1.1**](https://github.com/MikeMcl/bignumber.js/tree/v8.1.1)

## Sets with test values for `seed`, `p` and `q`

```sh
 Set 1
 
 p = 15485867
 q = 23878409
 seed = 8353
```

```sh
 Set 2
 
 p = 31415821
 q = 772531
 seed = 1878892440
```

```sh
 Set 3
 
 p = 3264011
 q = 472990951
 seed = 131893
```

```sh
 Set 4
 
 p = 3263849
 q = 1302498943
 seed = 6367859
```

```sh
 Set 5
 
 p = 3264011
 q = 472990951
 seed = 9883
```

```sh
 Set 6
 
 p = 4990364893
 q = 58105693
 seed = 6681193
```

```sh
 Set 7
 
 p = 20359
 q = 43063
 seed = 1993
```

```sh
 Set 8
 
 p = 87566873
 q = 15485143
 seed = 193945
```

```sh
 Set 9
 
 p = 87566873
 q = 15485143
 seed = 740191
```

```sh
 Set 10
 
 p = 87566873
 q = 5631179
 seed = 191
```

## References

* [Sets of Values](http://wiki.fib.upc.es/sim/index.php/Blum_Blum_Shub)
* [Algorithm Theory](https://en.wikipedia.org/wiki/Blum_Blum_Shub)

## License

MIT

**Free Software**